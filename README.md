# angular-prototype

You will need ruby, npm , grunt and compass to run this project.

## Build & development

Run: 
`npm install`
`bower install`
`grunt font-awesome`
`grunt serve`

## Testing

Running `grunt test` will run the unit tests with karma.