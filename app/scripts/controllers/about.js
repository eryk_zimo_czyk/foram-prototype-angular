'use strict';

/**
 * @ngdoc function
 * @name angularPrototypeApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angularPrototypeApp
 */
angular.module('angularPrototypeApp')
  .controller('AboutCtrl', function ($scope) {});
